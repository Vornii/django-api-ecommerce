from rest_framework import serializers
from . models import *
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
      model = User
      fields = ['username', 'email']
class ImageSerializer(serializers.ModelSerializer):
    class Meta:
       model = Images
       fields = [ 'id','images']
class CategorySerializerV2(serializers.ModelSerializer):
  
   class Meta:
      model = Category
      fields = ['id', 'categoryname']
class ProductSerializerV2(serializers.ModelSerializer):
    category =  CategorySerializerV2(many=False,read_only=False)
    owner = UserSerializer(many=False)
    class Meta:
      model = Product
      fields ='__all__'





class ProductSerializer(serializers.ModelSerializer):

    class Meta:
      model = Product
      fields ='__all__'


class CategorySerializer(serializers.ModelSerializer):
   product =  ProductSerializer(many=True,read_only=True)
   class Meta:
      model = Category
      fields = ['id', 'categoryname','product']

class OrderProductSerializer(serializers.ModelSerializer):
    
    product = ProductSerializer()

    class Meta:
        model = OrderProduct
        fields = ['product', 'quantity']



class OrderDetailSerializer(serializers.ModelSerializer):

   products = OrderProductSerializer(source='orderproduct_set', many=True)


   class Meta:
      model =OrderDetail

      read_only_fields = ('amount','products',)

      fields = '__all__'

class OrderDetailStatusSerializer(serializers.ModelSerializer):
    
    class Meta:

      model =OrderDetail

      read_only_fields = ('customer','method','qty','amount','product',)

      fields = '__all__'



# class OrderSerializer(serializers.ModelSerializer):
#    class Meta:
#       model =Order
#       fields = '__all__'

class CustomerSerializerLogin(serializers.ModelSerializer):
  class Meta:
      model =Customer
      fields = ['username','password']

class CustomerSerializer(serializers.ModelSerializer):

   class Meta:
      model =Customer
      fields = '__all__'
class AddressSerializer(serializers.ModelSerializer):
   customer_id  = CustomerSerializer(many=False,read_only= True)
   class Meta:
      model =Address
      fields = '__all__'
 
