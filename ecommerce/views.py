from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.urls import reverse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from . serializers import *
from rest_framework import generics
from rest_framework import status
from rest_framework.permissions import IsAdminUser,IsAuthenticatedOrReadOnly
from django.contrib.auth.hashers import make_password
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.status import *
from django.shortcuts import get_object_or_404,get_list_or_404
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import send_mail
import stripe
from django.conf import settings

from decimal import Decimal
# Create your views here.

# @api_view(['GET'])
# def getproduct(request,pk):
#     data:{}
#     query_set = Product.objects.all()
#     print(pk)
#     print(request.method)
#     print(request.GET.get('q'))

#     if query_set.exists():
#      serializer = ProductSerializer(    query_set,many=True)
#      return Response({
#         "message":    serializer.data
#      })

   
#     return Response({
#         "message":   'product not found'
#      })

class ProductList(generics.ListAPIView):
    serializer_class = ProductSerializerV2
    queryset = Product.objects.all()

    def get_queryset(self):
        print(super().get_queryset())
        q = get_list_or_404(Product)
   
        return   q 
class ProductRUD(generics.RetrieveUpdateDestroyAPIView)   :
   queryset = Product.objects.all() 
   # lookup_field = ['']
   # permission_classes = [IsAuthenticatedOrReadOnly]
   serializer_class = ProductSerializer
   def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({"status": "success", "code": status.HTTP_200_OK,
                        "message": "product has deleted successfully"}, status=status.HTTP_200_OK)


class ProductCreate(generics.CreateAPIView):
    serializer_class = ProductSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]



class CategoryCreate(generics.CreateAPIView):
 serializer_class =CategorySerializerV2

class CategoryList(generics.ListAPIView):
   serializer_class = CategorySerializer
   queryset = Category.objects.all()
class CategoryRUD(generics.RetrieveUpdateDestroyAPIView):
   serializer_class = CategorySerializer
   queryset = Category.objects.all()

   def destroy(self, request, *args, **kwargs):
      super().destroy(request, *args, **kwargs)
      return Response({"status": "success", "code": status.HTTP_200_OK,
                        "message": "category deleted successfully"}, status=status.HTTP_200_OK)

class OrderDetailCreate(generics.CreateAPIView):
   serializer_class =  OrderDetailSerializer
    
   def perform_create(self, serializer):
    print(self.request.data)
    data = self.request.data

        # Create the order
   #  order = OrderDetail.objects.create(customer = serializer.validated_data['customer'])
    total = 0
    order = OrderDetail.objects.create(
       customer = serializer.validated_data['customer'],  
         amount = total
         )

        # Process each product
    for product_data in data['products']:
       product = Product.objects.get(id=product_data['id'])
       quantity = product_data['quantity']
       # 5


       #3 

            # Check if enough stock is available
       if product.stockqty < quantity:
                return Response({"error": f"Not enough stock available for product {product.id}"})

            # Create the OrderProduct
         # Create the OrderProduct
       OrderProduct.objects.create(order=order, product=product, quantity=quantity)
       total +=   (quantity * product.price)
         

        # Update the stock quantity of the product
       product.stockqty -= quantity
       product.save()
 
      #  serializer = self.get_serializer(order)
      #  return Response(serializer.data) 
    order.amount = total
    order.save()
  
    send_mail(
    f"Order {   order.id}",
    """
  Your order has been placed
  thanks your for your order
""",
    "Nightpp19@gmail.com",
     [ order.customer.email ],
     fail_silently=False,
   )
    return Response(serializer.data)  


class OrderDetailCreateV2(generics.CreateAPIView):
   serializer_class = OrderDetailSerializer
    
   def perform_create(self, serializer):
    print("hello")
    pk =  self.kwargs.get('pid',None)


    pro = Product.objects.get(pk = pk)
    pro.stockqty = pro.stockqty - serializer.validated_data['qty']
    total = pro.price * serializer.validated_data['qty']
    pro.save()

    obj = serializer.save(customer = serializer.validated_data['customer'],amount =    total,product =  pro )

    useremail = obj.customer.email


    instance = Order.objects.create(order_id = obj.id, customer = serializer.validated_data['customer'] )
    send_mail(
    f"Order {instance.order_id}",
    """
Your order has been placed
thanks your for your order
""",
    "Nightpp19@gmail.com",
     [ useremail ],
     fail_silently=False,
   )
     

class OrderDetailRetriandDelete(generics.GenericAPIView):
   def get(self,request,pk):
      order = get_object_or_404(OrderDetail,pk=pk)
      serializers = OrderDetailSerializer(order,many=False)
      return Response(serializers.data,status=HTTP_200_OK)

   def delete(self,request,pk):
     if pk is not None:
      serializers =  OrderDetailStatusSerializer(data=request.data)
      order = get_object_or_404(OrderDetail,pk=pk)
      if serializers.is_valid():

       order.delete()
       return Response({
          'message':'Order has deleted successfully',
          'success':'ok',
          'status':HTTP_200_OK
       },status=HTTP_200_OK)  
     else:
       return Response(serializers.errors,status=HTTP_400_BAD_REQUEST)  

      


@api_view(['PUT','DELETE'])     
def OrderStatus(request,pk):
   if request.method == 'PUT':

    if pk is not None:
     serializers =  OrderDetailStatusSerializer(data=request.data)
     order = get_object_or_404(OrderDetail,pk=pk)
    if serializers.is_valid():
   
       order.status = serializers.validated_data['status']

 
       if(order.method =="card") :
         pass
       else:
         if(serializers.validated_data['status'] =='Delivered'):
          order.ispaid = True
         else:
           order.ispaid = False
       order.save()
       return Response(serializers.data,status=HTTP_200_OK)  
    else:
       return Response(serializers.errors,status=HTTP_400_BAD_REQUEST)  


   else:
     return Response({
        "error":"please provide your parameter value"
     },status=HTTP_400_BAD_REQUEST)



class OrderDetailView(generics.ListAPIView):
   serializer_class = OrderDetailSerializer
   queryset = OrderDetail.objects.all()
class AddressList(generics.ListAPIView):
   queryset= Address.objects.all()
   serializer_class = AddressSerializer

class RetrieveCustomAddress(APIView):

   def get(self,request,*arg,**kwargs):
      customer =get_object_or_404(Customer,pk=kwargs.get('pk',None))
      if customer is not None:
       addr = get_object_or_404(Address,customer_id=customer)
     
       serializers = AddressSerializer(addr)
         # Generate confirmation token and activation link
       confirmation_token = default_token_generator.make_token(user)
       activate_link_url = reverse('activate')
       activation_link = f'{activate_link_url}?user_id={user.id}&confirmation_token={confirmation_token}'

       return Response(serializers.data)
      else:
       return Response({
          "detail":"there is no related customer with by that id"
       },status=HTTP_404_NOT_FOUND)


class AddressSingle(generics.RetrieveUpdateDestroyAPIView):
   queryset= Address.objects.all()
   serializer_class = AddressSerializer

   def destroy(self, request, *args, **kwargs):
      super().destroy(request, *args, **kwargs)
      return Response({"status": "success", "code": status.HTTP_200_OK,
                        "message": "Address has deleted successfully"}, status=status.HTTP_200_OK)


# class AddressUser(generics.RetrieveUpdateAPIView):
#    queryset= Address.objects.all()
#    serializer_class = AddressSerializer
#    # def get(self, request, *args, **kwargs):
  

#    #     pk = self.kwargs.get('pk',None)
#    #     print(pk)
#    #     if pk is not None:
#    #       pk =  self.kwargs.get('pid',None)
#    #       customer =Customer.objects.get(pk=pk)
#    #       print(customer)

#    #       return Address.objects.get(customer_id  =customer)
#    #     else:
#    #        return Response({
#    #           'response':'no detail'
#    #        })


   def get(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)
       
       
   
class AddressCreate(generics.CreateAPIView):
   serializer_class = AddressSerializer
   def perform_create(self, serializer):
        # Perform additional actions before saving
        pk =  self.kwargs.get('pid',None)
        customer =get_object_or_404(Customer,pk = pk)
        print(pk)
        serializer.save(customer_id = customer  )
class ImageCreate(generics.ListCreateAPIView):
   serializer_class = ImageSerializer
   queryset = Images.objects.all()


   def get_queryset(self):
      instance =get_list_or_404(Images)
      return instance



   

   def create(self, request, *args, **kwargs):
      instance = super().create(request, *args, **kwargs)
      print(self.request.data)
      print(instance.data)
      return Response(
         {
            'success':'true',
            'message':'your image has been uploaded',
            'status':HTTP_200_OK,
            'url':instance.data
         },
         status=HTTP_200_OK
      )
     
class ImageRUD(generics.RetrieveAPIView):
   serializer_class = ImageSerializer
   queryset = Images.objects.all()
class ImageRUD(generics.RetrieveUpdateDestroyAPIView):
  serializer_class = ImageSerializer
  queryset = Images.objects.all()


# class CustomTokenObtainPairView(TokenObtainPairView):
#     serializer_class = CustomTokenObtainPairSerializer

@api_view(['POST'])
def logincustomer(request):
    serializers =  CustomerSerializerLogin(data=request.data)

    if serializers.is_valid():       


        try:
         user = Customer.objects.get(username = serializers.validated_data['username'], is_activated = True)
         refresh = RefreshToken.for_user( user )
         token = {
      'refresh': str(refresh),
      'access': str(refresh.access_token),
          }
         return Response(token)
        except Customer.DoesNotExist:
          return Response({
             "data":"there is no user associated with please create an account"
          },status=HTTP_400_BAD_REQUEST)
      

       
    else:
       #validation error
       return Response(serializers.errors,status=HTTP_400_BAD_REQUEST)



@api_view(['POST'])      
def register(request):
    print("register")

    serializers = CustomerSerializer(data=request.data)

    query_set = Customer.objects.filter(email = request.data['email'])
    if query_set.exists():
      return Response ({
                "error":"Email has already exist, try a new one"
             },status =HTTP_401_UNAUTHORIZED)
    if serializers.is_valid():
      olduser = Customer.objects.filter(email=serializers.validated_data['email'])
      print(olduser)
      if len(olduser) == 0:
             
        serializers.save()
        newuser = Customer.objects.get(email = serializers.validated_data['email'])
        hashed_password = make_password( newuser.password)
        newuser.password = hashed_password
    

        newuser.save()
  
        confirmation_token = default_token_generator.make_token(newuser)
        activate_link_url = reverse('activate')
        activation_link = f'{activate_link_url}?user_id={newuser.id}&confirmation_token={confirmation_token}'

        # Send email with activation link
        host = request.get_host()

        send_mail(
            'Activate your account',
            f'Please click on the following link to activate your account: {host}/{activation_link}>',
            'Nightpp19@gmail.com',
            [newuser.email],
            fail_silently=False,
      )
   
   #      refresh = RefreshToken.for_user(newuser)
   #      token = {
   #      'refresh': str(refresh),
   #      'access': str(refresh.access_token),
   #   }
      #   return Response(token)
       
        return Response( {
           "message":"An email has sent to your associated account"
        }, status=status.HTTP_201_CREATED)

    
  
  
  
      else:
             raise ValidationError({
                "error":"user already registered"
             })
    else:
     return Response(serializers.errors)
    

    